# PHP Coding Standards
### Mattheu Farmer Web Development

## About

This is the coding standards that Mattheu Farmer Web Development will strive to adhere to in any and all PHP code we produce.

## Link

You can see our coding standards on the [wiki](https://bitbucket.org/mfwebdev/php-coding-standards/wiki/) for this repository.